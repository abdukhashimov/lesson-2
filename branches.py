from subprocess import Popen
import subprocess


f = open("usernames.txt", "r")
usernames = f.read()
usernames = usernames.split('\n')
userNamesWithUnderscode = []

for username in usernames:
    userNamesWithUnderscode.append('_'.join(username.lower().split(' ')))

for username in userNamesWithUnderscode:
    # git branch username
    subprocess.run(["git", "branch", "{}".format(username)])
